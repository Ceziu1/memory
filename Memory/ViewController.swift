//
//  ViewController.swift
//  Memory
//
//  Created by qq_mietek on 07.09.2016.
//  Copyright © 2016 qq_mietekmiszc_kodzenia. All rights reserved.
//

import UIKit

extension Array {
    mutating func shuffle() {
        if count < 2 { return }
        for i in 0..<(count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
    //print()
}

class ViewController: UIViewController {
    var hard : [Int] = [0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,5,5,6,6,7,7]
    var click : Int = 0
    var score : Int = 0
    var clicked : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let cel = segue.destinationViewController as! Gmae
        let sz = UIScreen.mainScreen().bounds
        hard.shuffle()
        
        var margines : Int
        if segue.identifier == "e" {
            hard = [0,0,0,0,1,1,1,1,2,2,2,2]
            hard.shuffle()
            margines = (Int(sz.height) - Int(UIApplication.sharedApplication().statusBarFrame.height
) + Int((navigationController?.navigationBar.bounds.height)!) - (4 * (Int(sz.width)/3-10)))/2
            var temp : Int = 0
            for i in 0...2 {
                for j in 0...3 {
                    let button : UIButton = UIButton()
                    button.frame = CGRect(x: (i*Int(sz.width)/3)+2, y: j*Int(sz.width)/3 + margines, width: Int(sz.width)/3-4, height: Int(sz.width)/3-4)
                    button.setBackgroundImage(UIImage(named: "img/reverse.jpg"), forState: .Normal)
                    button.tag = temp
                    button.addTarget(self, action: #selector(pokaz), forControlEvents: .TouchUpInside)
                    cel.view.addSubview(button)
                    temp+=1
                }
            }
        } else {
            hard = [0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,5,5,6,6,7,7]
            hard.shuffle()
            margines = (Int(sz.height) - Int(UIApplication.sharedApplication().statusBarFrame.height
                ) + Int((navigationController?.navigationBar.bounds.height)!) - (6 * (Int(sz.width)/4-8)))/2//do poprawy
            var temp : Int = 0
            for i in 0...3 {
                for j in 0...5 {
                    let button : UIButton = UIButton()
                    button.frame = CGRect(x: i*Int(sz.width)/4+2, y: j*Int(sz.width)/4 + margines, width: Int(sz.width)/4-4, height: Int(sz.width)/4-4)
                    button.setBackgroundImage(UIImage(named: "img/reverse.jpg"), forState: .Normal)
                    button.tag = temp
                    button.addTarget(self, action: #selector(pokaz), forControlEvents: .TouchUpInside)
                    cel.view.addSubview(button)
                    temp+=1
                }
            }
        }
    }
    
    func pokaz(sender: UIButton){
        click+=1
        if click > 2 {
            return
        }
        //print(sender.tag)
        switch hard[sender.tag] {
        case 0:
            sender.setBackgroundImage(UIImage(named: "img/apple.png"), forState: .Normal)
            break;
        case 1:
            sender.setBackgroundImage(UIImage(named: "img/banana.png"), forState: .Normal)
            break;
        case 2:
            sender.setBackgroundImage(UIImage(named: "img/melon.png"), forState: .Normal)
            break;
        case 3:
            sender.setBackgroundImage(UIImage(named: "img/onion.png"), forState: .Normal)
            break;
        case 4:
            sender.setBackgroundImage(UIImage(named: "img/plum.png"), forState: .Normal)
            break;
        case 5:
            sender.setBackgroundImage(UIImage(named: "img/granade.png"), forState: .Normal)
            break;
        case 6:
            sender.setBackgroundImage(UIImage(named: "img/marakuya.png"), forState: .Normal)
            break;
        case 7:
            sender.setBackgroundImage(UIImage(named: "img/duda.png"), forState: .Normal)
            break;
        default:
            break;
        }
        if click==2 {
            if hard[clicked!.tag] == hard[sender.tag] {
                print("To samo")
                sender.userInteractionEnabled = false
                clicked?.userInteractionEnabled = false
                score+=2
            } else {
                sender.userInteractionEnabled = true
                clicked?.userInteractionEnabled = true
                print("Nie trafiłeś")
            }
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                if self.hard[self.clicked!.tag] != self.hard[sender.tag]{
                    self.clicked!.setBackgroundImage(UIImage(named: "img/reverse.jpg"), forState: .Normal)
                    sender.setBackgroundImage(UIImage(named: "img/reverse.jpg"), forState: .Normal)
                }
                self.clicked = nil
                self.click = 0
            }
        } else{
            clicked = sender
            clicked?.userInteractionEnabled = false
        }
        if score == hard.count{
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                let vc : UIViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("w2"))!
                self.showViewController(vc, sender: vc)
            }
            print("Koniec")
        }
    }

}

